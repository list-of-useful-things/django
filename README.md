[Django](https://www.djangoproject.com/download/) is a high-level Python Web framework that encourages rapid development and clean, pragmatic design. Built by experienced developers, it takes care of much of the hassle of Web development, so you can focus on writing your app without needing to reinvent the wheel. It’s free and open source.

# General links
* <b>[Awesome Django](https://github.com/wsvincent/awesome-django)</b>
 
# Libraries/frameworks/software
### Django-based projects
* [Oscar](https://github.com/django-oscar/django-oscar) - Domain-driven e-commerce for Django
* [Pinax](https://github.com/pinax/pinax) - a Django-based platform for rapidly developing websites 
* [Mezzanine](http://mezzanine.jupo.org/) - CMS
* [Wagtail](https://wagtail.io) - Wagtail is a leading open source CMS.
* [Saleor](https://github.com/mirumee/saleor) - A modular, high performance e-commerce storefront built with GraphQL, Django, and ReactJS. 
* [Online Banking System](https://github.com/saadmk11/banking-system) - A banking System Created Using Django Python Web Framework
 
### Uncategorized 
* [Django Packages](https://djangopackages.org/categories/projects/)
* [Tornado](https://github.com/tornadoweb/tornado) - Tornado is a Python web framework and asynchronous networking library
* [djangox](https://github.com/wsvincent/djangox) - A framework for launching new Django projects quickly.
* [Open EdX](https://github.com/edx/edx-platform) - The Open edX platform, the software that powers edX! 
* [Taiga](https://github.com/taigaio/taiga-back) - Project management web application with scrum in mind! Built on top of Django and AngularJS (Backend Code)
* [Workout Manager. wger](https://github.com/wger-project/wger) - Self hosted FLOSS fitness/workout and weight tracker written with Django
* [Review Board](https://github.com/reviewboard/reviewboard) - an open source, web-based code and document review tool built to help companies, open source projects, and other organizations keep their quality high and their bug count low.
* [Django-CRM](https://django-crm.readthedocs.io/en/latest/) / [github](https://github.com/MicroPyramid/Django-CRM) / [demo](https://demo.django-crm.io/login/?next=/) - Django-CRM provides a dashboard where you can manage customers at sales of the organization. It Provides to manage leads information and its activity, track issues from leads, contacts to send emails.
* [Simple Django Classified Advertising App](https://github.com/inoks/django-classified)
* [Askbot](https://github.com/ASKBOT/askbot-devel) - open source Q&A system, like StackOverflow, Yahoo Answers and some others. Askbot is based on code of CNPROG, originally created by Mike Chen and Sailing Cai and some code written for OSQA.
* [django-fobi](https://github.com/barseghyanartur/django-fobi/) - Form generator/builder application for Django done right: customisable, modular, user- and developer- friendly.
* [bootstrap-fileinput](https://github.com/kartik-v/bootstrap-fileinput) - An enhanced HTML 5 file input for Bootstrap 3.x with file preview, multiple selection, and more features.

# Apps
<b>Debug, logging</b>
* [Django Debug Toolbar](https://github.com/jazzband/django-debug-toolbar)</b> - The Django Debug Toolbar is a configurable set of panels that display various debug information about the current request/response and when clicked, display more details about the panel's content.

<b>Views, Templates</b>
* [crispy-forms](https://django-crispy-forms.readthedocs.io/en/latest/install.html) - a Django application that lets you easily build, customize and reuse forms using your favorite CSS framework, without writing template code and without having to take care of annoying details.
* [django-extra-views](https://github.com/AndrewIngram/django-extra-views) - a Django package which introduces additional class-based views in order to simplify common design patterns such as those found in the Django admin interface.
* [django-tables2](https://django-tables2.readthedocs.io/en/latest/pages/installation.html) - simplifies the task of turning sets of data into HTML tables. It has native support for pagination and sorting. It does for HTML tables what django.forms does for HTML forms. e.g.
* [django-easy-select2](https://github.com/asyncee/django-easy-select2) - Select2 input widget for django selectable fields. 
* [django-select2](https://github.com/applegrew/django-select2) - This is a Django integration for Select2

<b>Fields (Model extensions)</b>
* [django-colorful](https://github.com/charettes/django-colorful) - Extension to the Django web framework that provides database and form color fields. [Works with Django 2.2.4 and Bootstrap4 which I cannot say about other color picker apps for Django]
* [django-widget-tweaks](https://github.com/jazzband/django-widget-tweaks) - Tweak the form field rendering in templates, not in python-level form definitions. Altering CSS classes and HTML attributes is supported.
* [django-localflavor](https://github.com/django/django-localflavor) - Country-specific Django helpers, formerly of contrib fame
* [django-mirage-field](https://github.com/luojilab/django-mirage-field) - Django model field encrypt/decrypt your data, keep secret in database.
* [django-money](https://github.com/django-money/django-money) - Money fields for django forms and models
* [DjangoCon 2019 - Building a custom model field from the ground up](https://www.youtube.com/watch?v=g12fkl9NCrM)
 
<b>Data, CSV, Import/Export</b>
* [django-import-export](https://github.com/django-import-export/django-import-export) - Django application and library for importing and exporting data with admin integration
* [django-csvimport](https://github.com/edcrewe/django-csvimport) - Generic importer tool to allow the upload of CSV files for populating data. 
* [django-data-wizard](https://github.com/wq/django-data-wizard) - Import structured data (e.g. Excel, CSV, XML, JSON) into one or more Django models via an interactive web-based wizard
* [django-adaptors](https://github.com/anthony-tresontani/django-adaptors) - Convert CSV/XML files into python object or django model
 
<b>Admin-related extensions</b>
* [django-admin-steroids](https://gitlab.com/chrisspen/django-admin-steroids) - Tweaks and tools to simplify Django admin.
* [django-hijack](https://github.com/arteria/django-hijack) - With Django Hijack, admins can log in and work on behalf of other users without having to know their credentials.

<b>Authentication</b>
* [django-allauth](https://github.com/pennersr/django-allauth) - Integrated set of Django applications addressing authentication, registration, account management as well as 3rd party (social) account authentication
 
<b>Other</b>
* [django-scheduler](https://github.com/llazzaro/django-scheduler) - A calendaring app for Django.
* [django-front](https://github.com/mbi/django-front) - Django-front is a front-end editing application: placeholders can be defined in Django templates, which can then be edited on the front-end.
* [django-filter](https://github.com/carltongibson/django-filter) - Django-filter is a reusable Django application allowing users to declaratively add dynamic QuerySet filtering from URL parameters.
* [django-boost](https://github.com/ChanTsune/Django-Boost) - Extension library to boost development with django
* [django-querysetsequence](https://github.com/percipient/django-querysetsequence) - Chain multiple (disparate) QuerySets in Django
* [django-jutil](https://github.com/kajala/django-jutil) - Collection of small utilities for Django and Django REST framework projects.
* [Django Data Tables](https://gitlab.brolabs.de/pwach/django_data_tables) - Django Data Tables is a Framework for Django that creates model related Data Tables that fetch filtered data via AJAX Requests and allow different actions on model instances.
* [django-invoicing](https://github.com/PragmaticMates/django-invoicing) - Django app for invoicing
* [django-dynamic-attachments](https://github.com/imsweb/django-dynamic-attachments) - Django application for managing attachments to arbitrary models, with properties and multiple file uploads.


<b>Background task runners</b>
* [django-cron](https://github.com/Tivix/django-cron) - Write cron business logic as a Python class and let this app do the rest! It enables Django projects to schedule cron tasks, tracks their success / failures, manages contention (via a cache) etc. Basically takes care of all the boring work for you :-)
* [django-background-tasks](https://github.com/arteria/django-background-tasks) - A database-backed work queue for Django
* [huey (Django-biding)](https://github.com/coleifer/huey) - a little task queue for python
* [apscheduler](https://github.com/agronholm/apscheduler) - Task scheduling library for Python

# Templates
* <b>[Awesome Django Admin](https://github.com/originalankur/awesome-django-admin)</b> - Curated List of Awesome Django Admin Panel Articles, Libraries/Packages, Books, Themes, Videos, Resources.
* [wemake-django-template](https://github.com/wemake-services/wemake-django-template) - Bleeding edge django template focused on code quality and security
* [Django 1.9+ project template](https://jpadilla.github.io/django-project-template/)
* [Viewflow](http://viewflow.io/) / [demo](http://demo.viewflow.io/)
* [Grappelli](https://grappelliproject.com/)
* [Django Suit](https://djangosuit.com/) / [demo](https://v2.djangosuit.com/admin/)
* [django-admin-bootstrap](https://github.com/douglasmiranda/django-admin-bootstrap)
* [django-baton](https://github.com/otto-torino/django-baton)
* [XAdmin](https://github.com/sshwsfc/xadmin)
* <b>[AdminLTE2](https://github.com/adamcharnock/django-adminlte2)</b>

# Databases
### PostgreSQL
* [How to Use a Postgres Database in Django](https://www.youtube.com/watch?v=t6RbanOhna4)
* [How to use Django, PostgreSQL, and Docker](https://wsvincent.com/django-docker-postgresql/)
* [How To Use PostgreSQL with your Django Application on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04)
* [Optimizing Postgres full text search in Django](https://dev.to/danihodovic/optimizing-postgres-full-text-search-with-django-42hg)

# Django + Frontend frameworks
### React
* [VIDEO / React & Django TUTORIAL Integration // REACTify Django](https://www.youtube.com/watch?v=AHhQRHE8IR8) - Coding Entrepreneurs tutorial
* [Serving React and Django together](https://hackernoon.com/serving-react-and-django-together-2089645046e4)

### Vue
* [Integrate Django and Vue.js](https://johnfraney.ca/posts/2019/05/14/integrate-django-vuejs/)
* [Django Vue Template](https://github.com/gtalarico/django-vue-template) - minimal example for an application using Vue and Django
* [How to build a File Manager Storage web app with DRF and Vue.js](https://medium.com/js-dojo/how-to-build-a-file-manager-storage-web-app-with-django-rest-framework-and-vue-js-e89a83318e9c)
* [VIDEO / Let's Make an RSS Reader (Django and Vue)](https://www.youtube.com/watch?v=0FTaWat_VsM) - Video tutorial

### Angular
* [How to integrate Django REST Framework and Angular](https://www.netedge.plus/blog/how-to-integrate-django-rest-framework-and-angular)

# Docker
* [docker-django](https://github.com/erroneousboat/docker-django) - A project to get you started with Docker and Django.
* [How to use Django, PostgreSQL, and Docker](https://wsvincent.com/django-docker-postgresql/)

# API
### GraphQL
* [Graphene-Django](https://docs.graphene-python.org/projects/django/en/latest/) - Graphene-Django is built on top of Graphene. Graphene-Django provides some additional abstractions that make it easy to add GraphQL functionality to your Django project.
* [Turn your Django REST API into a GraphQL like API](https://blog.graphqleditor.com/turn-your-django-rest-api-into-a-graphql-like-api/)
* [Building Django HTTP APIs with GraphQL and Graphene](https://www.techiediaries.com/django-graphql-tutorial/)
* [Getting started with GraphQL in Python](https://blog.graphqleditor.com/getting-started-with-graphql-in-python/)

### REST
* [Tastypie](https://django-tastypie.readthedocs.io/en/latest/) - Tastypie is a webservice API framework for Django. It provides a convenient, yet powerful and highly customizable, abstraction for creating REST-style interfaces.
* [The Ultimate Tutorial for Django REST Framework: CRUD](https://sunscrapers.com/blog/ultimate-tutorial-django-rest-framework-part-1/)
* [10 things you need to know to effectively use Django Rest Framework](https://medium.com/profil-software-blog/10-things-you-need-to-know-to-effectively-use-django-rest-framework-7db7728910e0)
* [VIDEO / Getting Started With Django REST Framework](https://www.youtube.com/watch?v=263xt_4mBNc)
* [VIDEO / PyWaw #75 - Dobre praktyki w Django REST Framework](https://www.youtube.com/watch?v=vLvuBGLRPkg)

# Tutorials/Articles
### Examples
* [Book CRUD Application with Django](https://github.com/gdegirmenci/django-bookshelf) - 1 model, Django & Bootstrap 4 only. Simpla and nice.
* [CRUDL django example](https://github.com/crudlio/crudl-example-django) - This is a CRUDL example with Django, DRF (REST), Graphene (GraphQL) and SQLite.

### Preparing environment
* [Virtual Environment in Python - A Pocket Guide](https://www.pythoncircle.com/post/404/virtual-environment-in-python-a-pocket-guide/)
* [Virtual Environments](https://python-guide-cn.readthedocs.io/en/latest/dev/virtualenvs.html)
* [Helpful Bash Aliases for Django](https://coderwall.com/p/rer0za/helpful-bash-aliases-for-django)
* [Running Django in PyCharm Community](https://www.reddit.com/r/django/comments/cpbwuw/pycharm_community_edition_and_django/)

### Basics 
* <b>[Simple is better then complex - Article Archive](https://simpleisbetterthancomplex.com/archive/)</b>
* <b>[Django Best Practices](https://django-best-practices.readthedocs.io/en/latest/index.html)</b>
* [Starting with Django](https://www.pythoncircle.com/post/26/hello-word-in-django-how-to-start-with-django/) - How to start building the project structure, where to start from etc. Basically hello world of Django
* [A Complete Beginner's Guide to Django](https://simpleisbetterthancomplex.com/series/beginners-guide/1.11/)
* [Where to put business logic in Django (tech talk)](https://sunscrapers.com/blog/where-to-put-business-logic-django/)
* [Imports](https://wsvincent.com/django-tips-imports/)
* [ManyToManyField](https://www.revsys.com/tidbits/tips-using-djangos-manytomanyfield/)
* [Top 10 Mistakes that Django Developers Make](https://www.toptal.com/django/django-top-10-mistakes)
* [Things You Must Know About Django Admin As Your App Gets Bigger](https://medium.com/@hakibenita/things-you-must-know-about-django-admin-as-your-app-gets-bigger-6be0b0ee9614)
* [How To Debug in Django. Useful Tips](https://www.bedjango.com/blog/how-debug-django-useful-tips/)
* [Demystifying Mixins with Django](https://www.youtube.com/watch?v=rMn2wC0PuXw)
* [Django 2.0 url() to path() cheatsheet](https://consideratecode.com/2018/05/02/django-2-0-url-to-path-cheatsheet/)

### Apps
* [Tips for Building High-Quality Django Apps at Scale](https://medium.com/@DoorDash/tips-for-building-high-quality-django-apps-at-scale-a5a25917b2b5)

### Models 
* <b>[Best practices working with Django models in python](https://medium.com/@SteelKiwiDev/best-practices-working-with-django-models-in-python-b17d98ab92b)</b>
* [Advanced Django Models](https://djangobook.com/advanced-models/)
* [Designing Better Models](https://simpleisbetterthancomplex.com/tips/2018/02/10/django-tip-22-designing-better-models.html)
* [Avoid Django’s GenericForeignKey](https://simpleisbetterthancomplex.com/tutorial/2016/10/13/how-to-use-generic-relations.html) - How to implement class with ForeignKey to either of two classes.

### Forms
* [Advanced Forms (Crispy Forms)](https://simpleisbetterthancomplex.com/tutorial/2018/11/28/advanced-form-rendering-with-django-crispy-forms.html)
* [Alternative to django-crispy-forms](https://stackoverflow.com/questions/55072876/alternative-to-django-crispy-forms)
* [Django inline formsets with Class-based views and crispy forms](https://dev.to/zxenia/django-inline-formsets-with-class-based-views-and-crispy-forms-14o6)
* [Django Crispy Forms - what are they about?](https://www.merixstudio.com/blog/django-crispy-forms-what-are-they-about/)
* [Django and AJAX Form Submissions – Say 'Goodbye' to the Page Refresh](https://realpython.com/django-and-ajax-form-submissions/)

### TEMPLATES
* [Writing Custom Contexts for Django](https://lethain.com/writing-custom-contexts-django/)

### Authentication/Authorization
* [How to use Google reCAPTCHA in Django](https://www.pythoncircle.com/post/183/how-to-use-google-recaptcha-in-django/)
* [Django Tutorial: Building and Securing Web Applications](https://auth0.com/blog/django-tutorial-building-and-securing-web-applications/?utm_source=twitter&utm_medium=sc&utm_campaign=djangotut_webapp) 
* [Lesson 1: A Step by Step Guide on How To Use Django 2.2 Authentication System and Python 3.7](https://blog.hlab.tech/lesson-1-a-step-by-step-guide-on-how-to-use-django-2-2-authentication-system-and-python-3-7/)
* [Lesson 2: A Step by Step Guide on How to Use Bootstrap and Register User in Django 2.2 Authentication System and Python 3.7](https://blog.hlab.tech/lesson-2-a-step-by-step-guide-on-how-to-use-bootstrap-and-register-user-in-django-2-2-authentication-system-and-python-3-7/)
* [Lesson 3. A Step by Step guide on Creating Custom Dashboard and Login User in Django 2.2 and Python 3.7.](https://blog.hlab.tech/lesson-3-a-step-by-step-guide-on-creating-custom-dashboard-and-login-user-in-django-2-2-and-python-3-7/)
* [William Vincent - Django Login/Logout Tutorial (Part 1)](https://wsvincent.com/django-user-authentication-tutorial-login-and-logout/)
* [William Vincent - Django Sign Up Tutorial (Part 2)](https://wsvincent.com/django-user-authentication-tutorial-signup/)
* [William Vincent - Django Password Reset Tutorial (Part 3)](https://wsvincent.com/django-user-authentication-tutorial-password-reset/)
* [William Vincent - Source code for Django authentication tutorial](https://github.com/wsvincent/django-auth-tutorial)

### Queries & Optimalization
* [Profiling and Django settings](https://dizballanze.com/en/django-project-optimization-part-1/)
* [Working with database](https://dizballanze.com/en/django-project-optimization-part-2/)
* [Caching](https://dizballanze.com/en/django-project-optimization-part-3/)
* [Select & Prefetch Related](https://medium.com/@lucasmagnum/djangotip-select-prefetch-related-e76b683aa457)
* [Understanding Django's prefetch_related helper](https://www.phizzle.space/django/orm/2016/03/08/understanding-django-prefetch-related.html)
* [Django ORM: what are its less obvious features?](https://www.merixstudio.com/blog/neat-features-django-orm/)
* [Using Django querysets effectively](https://blog.etianen.com/blog/2013/06/08/django-querysets/)
* [Digging Into Django QuerySets](https://www.caktusgroup.com/blog/2017/04/05/digging-into-django-querysets/)
* [Django Queries Optimization](https://medium.com/@valerybriz/django-queries-optimization-f5670e76d481)
* [How to Filter for Empty or Null Values in a Django QuerySet](https://chartio.com/resources/tutorials/how-to-filter-for-empty-or-null-values-in-a-django-queryset/)
* [YT - Django queries optimization by Ivaylo Donchev](https://www.youtube.com/watch?v=qDyxmgWkjvI)
* [YT - I Didn't Know Querysets Could do That by Charlie Guo](https://www.youtube.com/watch?v=5y7vU52jOiQ)
* [YT - Making smarter queries with advanced ORM resources](https://www.youtube.com/watch?v=eUM3b2q27pI)

### Testing
* [Blazing fast tests in Django](https://dizballanze.com/en/django-blazing-fast-tests/)

### Features
* [Email Subscription](https://www.pythoncircle.com/post/657/adding-email-subscription-feature-in-django-application/)
* [Email Tracking](https://www.pythoncircle.com/post/626/how-to-track-email-opens-sent-from-django-app/)
* [Custom Templates](https://www.pythoncircle.com/post/42/creating-custom-template-tags-in-django/)
* [Custom 404 Error Page](https://www.pythoncircle.com/post/564/displaying-custom-404-error-page-not-found-page-in-django-20/)
* [Favicon](https://www.pythoncircle.com/post/684/how-to-add-favicon-to-django-websites/)
* [Dynamic URLs](https://www.pythoncircle.com/post/584/creating-sitemap-of-dynamic-urls-in-your-django-application/)
* [Robots.txt](https://www.pythoncircle.com/post/578/adding-robotstxt-file-to-django-application/)
* [Custom User Model](https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html)
* [Custom User Model](https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#specifying-a-custom-user-model)
* [Websockets (Django Channels)](https://channels.readthedocs.io/en/latest/)
 
### Django Rest Framework
* [Testing](https://www.django-rest-framework.org/api-guide/testing/)
* [Filtering](https://github.com/philipn/django-rest-framework-filters)
* [Serializers / How to use serializers effectively](https://medium.com/@raaj.akshar/how-to-use-drf-serializers-effectively-dc58edc73998)
* [Serializers / Read & Write Serializers](https://www.vinta.com.br/blog/2018/django-rest-framework-read-write-serializers/)
* [Dynamic search fields](https://medium.com/@raaj.akshar/how-to-make-search-fields-dynamic-in-django-rest-framework-72922bfa1543)
* [VIDEO / Build a Rest API with the Django REST Framework](https://www.youtube.com/watch?v=tG6O8YF91HE) - Build a Rest API with the Django REST Framework and Python 3.6 from CodingEntrepreneurs
* [DRF EXAMPLE](https://github.com/athento/drf-example) - Project showcasing the capabilities of the Django REST Framework
* [DRF Tricks](https://github.com/barseghyanartur/django-rest-framework-tricks) - Collection of various tricks for Django REST framework.
* [DRF Generator](https://github.com/Brobin/drf-generators) - triangular_ruler Generate Views, Serializers, and Urls for your Django Rest Framework application
* [Classy Django REST Framework](http://www.cdrf.co/) - Detailed descriptions, with full methods and attributes, for each of Django REST Framework's class-based views and serializers.
* [Django REST framework view inheritance diagram](https://i.redd.it/ve66iak6gxm11.png)

### Videos
* [Python Django Web Framework - Full Course for Beginners](https://www.youtube.com/watch?v=F5mRW0jo-U4) - very good tutorial with total of 3h45
* [Python Django Tutorial: Full-Featured Web App Part 1 - Getting Started](https://www.youtube.com/watch?v=UmljXZIypDc)
* [Web Development in Python with Django](https://coreyms.com/development/python/python-django-tutorials-full-series) - tutorial by Corey Schafer
* [Django Tutorials](https://www.youtube.com/playlist?list=PLw02n0FEB3E3VSHjyYMcFadtQORvl1Ssj) - tutorial by Max Goodridge
* [Django Tutorials for Beginners - Creating a Website](https://www.youtube.com/playlist?list=PLCUJw_Sto3WTUPZwizZWBehPGqquQfHT2)
* [Django + Chart.js // Learn to intergrate Chart.js with Django](https://www.youtube.com/watch?v=B4Vmm3yZPgc)
* [Full Stack React & Django (1) - Basic REST API](https://www.youtube.com/watch?v=Uyei2iDA4Hs)
* [Pretty Prinited Django Tutorials](https://www.youtube.com/playlist?list=PLXmMXHVSvS-DQfOsQdXkzEZyD0Vei7PKf)
* [thenewboston Django Tutorials](https://www.youtube.com/watch?v=qgGIqRFvFFk&list=PL6gx4Cwl9DGBlmzzFcLgDhKTTfNLfX1IK)
* [JustDjango YouTube Channel](https://www.youtube.com/channel/UCRM1gWNTDx0SHIqUJygD-kQ)

### Hosting
* [How to host django app on pythonanywhere for free](https://www.pythoncircle.com/post/18/how-to-host-django-app-on-pythonanywhere-for-free/)
* [Automatically updating Django website hosted on PythonAnyWhere server with every git push](https://www.pythoncircle.com/post/261/automatically-updating-django-website-hosted-on-pythonanywhere-server-with-every-git-push/)
* [How to backup database periodically on PythonAnyWhere server](https://www.pythoncircle.com/post/360/how-to-backup-database-periodically-on-pythonanywhere-server/)

### Uncategorized
* [How to use AJAX with Django](https://www.pythoncircle.com/post/130/how-to-use-ajax-with-django/)
* [Understanding Django Deployment](https://erayerdin.hashnode.dev/understanding-django-deployment-cjvwj1gf4002u7us11db9r0u5)
* [11 Django Real-World Challenges Your Tutorial Didn't Mention](https://vsupalov.com/11-django-real-world-challenges/)
* [Make a Location-Based Web App With Django and GeoDjango](https://realpython.com/location-based-app-with-geodjango-tutorial/)
* [Django Tutorial: The Local Library website](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Tutorial_local_library_website)
* [Developing a Real-Time Taxi App with Django Channels and Angular](https://testdriven.io/courses/real-time-app-with-django-channels-and-angular/)
* [Setting up a Django project like a pro](https://medium.com/@fceruti/setting-up-a-django-project-like-a-pro-a847a9867f9d)
* [Django Stripe Tutorial](https://testdriven.io/blog/django-stripe-tutorial/) - how to configure a new Django website from scratch to accept one-time payments with Stripe
* [Setting up Stripe Connect with Django](https://testdriven.io/blog/setting-up-stripe-connect-with-django/) - This tutorial looks at how to integrate Stripe Connect into a Django application.
* [Behavior-Driven Development with Django and Aloe](https://testdriven.io/blog/behavior-driven-development-with-django-and-aloe/)
* [Small Open-Source Django Projects to Get Started](https://simpleisbetterthancomplex.com/2015/11/23/small-open-source-django-projects-to-get-started.html)
* [The Simplest WSGI Middleware](https://adamj.eu/tech/2019/05/27/the-simplest-wsgi-middleware/)
* [Sales Records API endpoint with Django Rest Framework and Django Filters with Docker](https://github.com/talented/Sales-Records-API)
* [Building an API with Django REST Framework and Class-Based Views](https://codeburst.io/building-an-api-with-django-rest-framework-and-class-based-views-75b369b30396?gi=8c4f9afceaec)
 
# Other
### Templates, Boilerplates
* [Cookiecutter Django-Vue](https://github.com/vchaptsev/cookiecutter-django-vue) - Cookiecutter Django Vue is a template for Django-Vue projects
* [Cookiecutter Django](https://github.com/pydanny/cookiecutter-django) - a framework for jumpstarting production-ready Django projects quickly. 
* [Cookiecutter Django-app](https://github.com/edx/cookiecutter-django-app) - a cookiecutter template for creating reusable Django packages (installable apps) quickly
* [Cookiecutter Django-REST](https://github.com/agconti/cookiecutter-django-rest) - REST + PostgreSQL + Docker
* [Cookiecutter Wemake-Django](https://github.com/wemake-services/wemake-django-template) - Bleeding edge django2.2 template focused on code quality and security.
* [DRF Typescript boilerplate](https://www.reddit.com/r/django/comments/ciyni7/django_django_rest_with_reacttypescript/)
* [Django + Bootstrap4](https://github.com/zostera/django-bootstrap4)

# To check
* [django-enumchoicefield](https://github.com/timheap/django-enumchoicefield)
* [Using Enum as Model Field Choice in Django](https://hackernoon.com/using-enum-as-model-field-choice-in-django-92d8b97aaa63)
* [Bootstrap File Input](https://mdbootstrap.com/docs/jquery/forms/file-input/)
* [Tips for Using Django's ManyToManyField](https://www.revsys.com/tidbits/tips-using-djangos-manytomanyfield/)
* [Migrating to a Custom User Model in Django](https://www.caktusgroup.com/blog/2013/08/07/migrating-custom-user-model-django/)
 
